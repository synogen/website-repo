<?php
/**
 * <Site Name> - <What this is>
 */
include_once "modules/site.variables.php";
include_once "modules/site.header.php";
$subtitle = "Page Name";
?>
    <title><?php print "{$title} - {$subtitle}";?></title>
</head>
<body>
<div id="wrapper" class="toggled">
    <?php include_once "modules/navigation.php";?>
    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 content-bx bx-dark text-sm-left">
                    <?php if (isset($_SESSION['logged_in'])) {
                        //page content here
                        ?>
                    <?php } else { include_once "modules/redirect.php"; }?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once "modules/site.footer.php"?>