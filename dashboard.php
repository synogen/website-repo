<?php
/**
 * Tournament Brackets - Accounts Dashboard
 */
include_once "modules/site.variables.php";
include_once "modules/site.header.php";
$this_file = __FILE__;
if(isset($_SESSION['user_type']))
    if ($_SESSION['user_type'] == "admin" || $_SESSION['user_type'] == "owner")
        $subtitle = "Accounts Dashboard";
else $subtitle = "Invalid Page Request";
if(isset($_POST['addUser'])) {
    if (isset($_POST['user_type']) && isset($_POST['username']) && isset($_POST['password']) && isset($_POST['first_name']) && isset($_POST['last_name']) && isset($_POST['email'])) {
        if (!$database_access->IsExistingUser($_POST['username'])) {
            if (!$database_access->IsRegisteredEmail($_POST['email'])) {
                $database_access->InsertNewUser(
                    $_POST['username'],
                    $_POST['password'],
                    $_POST['first_name'],
                    $_POST['last_name'],
                    $_POST['email'],
                    $_POST['user_type']);
            } else print "<script>alert(\"Email given for new account is already registered.\\nPlease enter an unregistered email.\");</script>";
        } else print "<script>alert(\"Username given for new account is already in use.\\nPlease enter a unique username.\");</script>";
    }
}
if(isset($_POST['change_user_info'])) {
    for ($i = 1; $i <= array_values(array_slice($_POST['user_id'], -1))[0]; $i++)
        if(isset($_POST['original_username'][$i]) &&
            ($_POST['original_username'][$i] !=  $_POST['username'][$i] || $_POST['original_user_type'][$i] != $_POST['user_type'][$i])) {
            if (!$database_access->IsExistingUser($_POST['username'][$i])) {
                $database_access->database->replace(
                    "users",
                    [
                        "username" =>
                            [
                                $_POST['original_username'][$i] => $_POST['username'][$i]
                            ],
                        "type" =>
                            [
                                $_POST['original_user_type'][$i] => $_POST['user_type'][$i]
                            ],
                    ],
                    [
                        "user_id" => $_POST['user_id'][$i]
                    ]
                );
            } else print "<script>alert(\"One or more username changes were the same username as existing users.\\nPlease enter usernames that are unique, and of non-existing users\");</script>";
        }
}
if(isset($_POST['user_id']))
    for ($i = 1; $i <= array_values(array_slice($_POST['user_id'], -1))[0]; $i++)
        if( isset($_POST['delete_user'][$i])) $database_access->database->delete("users", ["user_id" => $_POST['user_id'][$i]]);
?>
    <title><?php print "{$title} - {$subtitle}";?></title>
</head>
<body>
<div id="wrapper" class="toggled">
    <?php include_once "modules/navigation.php";?>
    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 content-bx bx-dark text-sm-left">
                    <?php
                    if (isset($_SESSION['logged_in']) && isset($_SESSION['user_type'])) {
                        if ($_SESSION['user_type'] == "admin" || $_SESSION['user_type'] == "owner") { ?>
                            <h1 class="text-sm-center"><?php print $subtitle;?></h1>
                            <form id="addUser" method="post" action="dashboard.php">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover">
                                        <thead class="thead-inverse">
                                        <tr>
                                            <th>Type</th>
                                            <th>Username</th>
                                            <th>Password</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Email</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <select name="user_type" form="addUser">
                                                    <option value="user" selected>User</option>
                                                    <?php
                                                    if (isset($_SESSION['user_type']))
                                                        if ($_SESSION['user_type'] == "owner")
                                                            print "<option value=\"admin\">Admin</option>"; ?>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" name="username" placeholder="Username..."/>
                                            </td>
                                            <td>
                                                <input type="text" name="password" placeholder="Password..."/>
                                            </td>
                                            <td>
                                                <input type="text" name="first_name" value="FName" placeholder="First Name..."/>
                                            </td>
                                            <td>
                                                <input type="text" name="last_name" value="LName" placeholder="Last Name..."/>
                                            </td>
                                            <td>
                                                <input type="email" name="email" placeholder="Email..."/>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <br>
                                <input class="btn-lg-dark float-sm-right" type="submit" name="addUser" value="Add New User"/>
                                <br class="clearfix">
                            </form>
                            <hr>
                            <?php $database_access->RenderUsersTable($database_access->GetTable("users"));
                        }
                     } else { print "Invalid Page Request."; }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once "modules/site.footer.php"?>