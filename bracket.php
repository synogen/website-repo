<?php
/**
 * Versus - Bracket Page
 */
include_once "modules/site.variables.php";
$this_file = __FILE__;
$tournament = null;
$bracket = null;
if (isset($_GET['id'])) {
    $tournament = $database_access->GetTableLastRow("tournaments", ["tournament_id" => $_GET['id']]);
    $bracket = $database_access->GetTable("tournament_brackets", ["tournament_id" => $_GET['id']]);
}
$totalParticipants = null;
$round = null;
$bracket_winner_id = null;
if($tournament != null && $bracket != null)
{
    $totalParticipants = $tournament['player_limit'];
    $round = 1;
    $i = $totalParticipants;
    //Calculate rounds
    do {
        $i = $i / 2;
        $round++;
    } while ($i != 1);
    $id = 0;
    foreach ($bracket as $player) {
        if ($player['bracket_win'] == 1) $bracket_winner_id = $id;
        else $id++;
    }
}
if(isset($_POST['r1_seed_win'])) {
    for ($i = 1; $i < $tournament['player_limit']; $i++){
        if(isset($_POST['r1_seed_win'][$i])
            && !empty($_POST['r1_seed_win'][$i])
            && ($_POST['r1_seed_win'][$i] == 1 || $_POST['r1_seed_win'][$i] == 0))
            $database_access->UpdatePlayerRoundWinResult($tournament['tournament_id'], $i, 1, $_POST['r1_seed_win'][$i]);
    }
    header("Location: bracket?id={$tournament['tournament_id']}");
}
if(isset($_POST['r2_seed_win'])) {
    for ($i = 1; $i < $tournament['player_limit']; $i++){
        if(isset($_POST['r2_seed_win'][$i])
            && !empty($_POST['r2_seed_win'][$i])
            && ($_POST['r2_seed_win'][$i] == 1 || $_POST['r2_seed_win'][$i] == 0))
            $database_access->UpdatePlayerRoundWinResult($tournament['tournament_id'], $i, 2, $_POST['r2_seed_win'][$i]);
    }
    header("Location: bracket?id={$tournament['tournament_id']}");
}
if(isset($_POST['r3_seed_win'])) {
    for ($i = 1; $i < $tournament['player_limit']; $i++){
        if(isset($_POST['r3_seed_win'][$i])
            && !empty($_POST['r3_seed_win'][$i])
            && ($_POST['r3_seed_win'][$i] == 1 || $_POST['r3_seed_win'][$i] == 0))
            $database_access->UpdatePlayerRoundWinResult($tournament['tournament_id'], $i, 3, $_POST['r3_seed_win'][$i]);
    }
    header("Location: bracket?id={$tournament['tournament_id']}");
}
if(isset($_POST['r4_seed_win'])) {
    for ($i = 1; $i < $tournament['player_limit']; $i++){
        if(isset($_POST['r4_seed_win'][$i])
            && !empty($_POST['r4_seed_win'][$i])
            && ($_POST['r4_seed_win'][$i] == 1 || $_POST['r4_seed_win'][$i] == 0))
            $database_access->UpdatePlayerRoundWinResult($tournament['tournament_id'], $i, 4, $_POST['r4_seed_win'][$i]);
    }
    header("Location: bracket?id={$tournament['tournament_id']}");
}
if(isset($_POST['final_seed_win'])) {
    for ($i = 1; $i < $tournament['player_limit']; $i++){
        $final_round = $round - 1;
        if(isset($_POST['final_seed_win'][$i])
            && !empty($_POST['final_seed_win'][$i])
            && ($_POST['final_seed_win'][$i] == 1 || $_POST['final_seed_win'][$i] == 0)) {
            $database_access->UpdatePlayerRoundWinResult($tournament['tournament_id'], $i, $final_round, $_POST['final_seed_win'][$i]);
            $database_access->UpdatePlayerRoundWinResult($tournament['tournament_id'], $i, $round, $_POST['final_seed_win'][$i]);
            $database_access->UpdatePlayerBracketWinResult($tournament['tournament_id'], $i, $_POST['final_seed_win'][$i]);
        }
    }
    header("Location: bracket?id={$tournament['tournament_id']}");
}
$subtitle = "{$tournament['tournament_name']}";
include_once "modules/site.header.php";
?>
    <title><?php print "{$title} - {$subtitle}";?></title>
    </head>
    <body>
<div id="wrapper" class="toggled">
    <?php include_once "modules/navigation.php";?>
    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="bx-dark-bracket text-xs-center" style="width: 965px; padding: 25px; margin-bottom: 15px; font-size: 1rem;">
                    <h1><?php print "{$tournament['tournament_name']}";?></h1>
                    <?php
                        print "<span class=\"font-weight-bold\">Oraganizer: </span><a class=\"text-muted-custom\" href=\"/profile?user={$database_access->GetUsername($tournament['tournament_id'])}\">{$database_access->GetUsername($tournament['tournament_id'])}</a><br/>";
                        print ($bracket_winner_id != null ? "<span class=\"font-weight-bold\">Winner:</span> {$bracket[$bracket_winner_id]['player_name']}&nbsp;&nbsp;" : "")
                        ."<span class=\"font-weight-bold\">{$tournament['activity_type']}:</span> {$tournament['activity_name']}".PHP_EOL;
                        if(isset($_SESSION['user_id']))
                        {
                            if ($_SESSION['user_id'] == $tournament['owner_id']) {
                                print "<br>";
                                print "<span class=\"font-weight-bold\" style='font-size: 0.75rem'>Note: </span><span class=\"text-muted-custom\" style='font-size: 0.75rem'>Please update only with finalized results.</span>";
                            }
                        }?>
                </div>
            </div>
            <div class="row">
                <div id="bracket" class="col-xs-12 bx-dark-bracket">
                    <form action="bracket.php?id=<?php print "{$tournament['tournament_id']}";?>" method="post">
                        <?php
                        if($tournament != null && $bracket != null) {
                            //for ($i = $totalParticipants; $i > 0.5; ($i / 2)) { $round++; }
                            for ($i = 1; $i < $round; $i++) {
                                $round_num = $i;
                                if ($round_num == 1) {
                                    print "<div class=\"round_{$round_num}\">";
                                    print "<div class=\"bx-dark text-xs-center\">Round {$round_num}</div>";
                                    if ($round_num % 2 == 0) print "<div class=\"bracket-space\"></div>";
                                    for ($j = 0; $j < (count($bracket) / 2); $j++) {
                                        $group_num = $j + 1;
                                        if ($round_num % 2 != 0) {
                                            print "<div class=\"bracket-space\"></div>".PHP_EOL;
                                        }
                                            print "<div class=\"group{$group_num}\">".PHP_EOL;
                                            print "<input type=\"hidden\" name=\"seed[{$bracket[$j]['player_seed']}]\" value=\"{$bracket[$j]['player_seed']}\"/>".PHP_EOL;
                                            print "<input type=\"hidden\" name=\"seed[{$bracket[(count($bracket)-$j-1)]['player_seed']}]\" value=\"{$bracket[(count($bracket)-$j-1)]['player_seed']}\"/>".PHP_EOL;
                                            print "<div id=\"seed_{$bracket[$j]['player_seed']}\" class=\"bx-dark-bracket\"><span class=\"d-inline-block float-xs-left\"><div class='seed_number'>{$bracket[$j]['player_seed']}</div> {$bracket[$j]['player_name']}</span>";
                                            if($_SESSION['user_id'] == $tournament['owner_id'] && $bracket[$j]["r{$round_num}_result"] != 1 && $bracket[(count($bracket)-$j-1)]["r{$round_num}_result"] != 1)
                                                print "<button class=\"float-xs-right\" type=\"submit\" name=\"r{$round_num}_seed_win[{$bracket[$j]['player_seed']}]\" value=\"1\">+</button></div>".PHP_EOL;
                                            else print "</div>".PHP_EOL;
                                            print "<div id=\"seed_{$bracket[(count($bracket)-$j-1)]['player_seed']}\" class=\"bx-dark-bracket\"><span class=\"d-inline-block float-xs-left\"><div class='seed_number'>{$bracket[(count($bracket)-$j-1)]['player_seed']}</div> {$bracket[(count($bracket)-$j-1)]['player_name']}</span>";
                                            if($_SESSION['user_id'] == $tournament['owner_id'] && $bracket[(count($bracket)-$j-1)]["r{$round_num}_result"] != 1 && $bracket[$j]["r{$round_num}_result"] != 1)
                                                print "<button class=\"float-xs-right\" type=\"submit\" name=\"r{$round_num}_seed_win[{$bracket[(count($bracket)-$j-1)]['player_seed']}]\" value=\"1\">+</button></div>".PHP_EOL;
                                            else print "</div>".PHP_EOL;
                                            print "</div>".PHP_EOL;
                                        if ($round_num % 2 == 0) {
                                            print "<div class=\"bracket-space\"></div>".PHP_EOL;
                                        }
                                    }
                                    if($round_num % 2 != 0) print "<div class=\"bracket-space\"></div>".PHP_EOL;
                                    print "</div>".PHP_EOL;
                                } else {
                                    print "<div class=\"round_{$round_num}\">";
                                    print "<div class=\"bx-dark text-xs-center\">Round {$round_num}</div>";
                                    if ($round_num % 2 == 0) print "<div class=\"bracket-space\"></div>".PHP_EOL;
                                    $round_win_index = null;
                                    $index_incrementer = 0;
                                    $prevRound = $round_num - 1;
                                    for ($j = 0; $j < (count($bracket) / 2); $j++)
                                    {
                                        if ($bracket[$j]["r{$prevRound}_result"] == 1) {
                                            $round_win_index[$index_incrementer] = $j;
                                            $index_incrementer++;
                                        }
                                        if ($bracket[(count($bracket)-$j-1)]["r{$prevRound}_result"] == 1) {
                                            $round_win_index[$index_incrementer] = (count($bracket)-$j-1);
                                            $index_incrementer++;
                                        }
                                    }
                                    $divsWritten = 0;
                                    $divsNeeded = $totalParticipants;
                                    $prevIndex = 0;
                                    for ($j = 0; $j < $round_num; $j++) {
                                        $divsNeeded /= 2;
                                    }
                                    if(count($round_win_index) > 0) {
                                        for ($j = 0; $j < count($round_win_index); $j++) {
                                            //$group_num = $round + ($round_num - 1) + ($j/2) ;
                                            //$group_num = $j + 1;
                                            IF ($j % 2 == 0) {
                                                if ($round_num % 2 != 0) {
                                                    print "<div class=\"bracket-space\"></div>";
                                                }
                                                print "<div>";
                                                print "<input type=\"hidden\" name=\"seed[{$bracket[$round_win_index[$j]]['player_seed']}]\" value=\"{$bracket[$round_win_index[$j]]['player_seed']}\">";
                                                print "<div id=\"seed_{$bracket[$round_win_index[$j]]['player_seed']}\" class=\"bx-dark-bracket\"><span class=\"d-inline-block float-xs-left\"><div class='seed_number'>{$bracket[$round_win_index[$j]]['player_seed']}</div> {$bracket[$round_win_index[$j]]['player_name']}</span>";
                                                if ($round_win_index[$j] == $round_win_index[count($round_win_index)-1]) print "</div>".PHP_EOL;
                                                elseif ($_SESSION['user_id'] == $tournament['owner_id'] && $bracket[$round_win_index[$j]]["r{$round_num}_result"] != 1 && $bracket[$round_win_index[$j+1]]["r{$round_num}_result"] != 1)
                                                    print "<button class=\"float-xs-right\" type=\"submit\" name=\"".($round_num == ($round-1) ? "final" : "r{$round_num}")."_seed_win[{$bracket[$round_win_index[$j]]['player_seed']}]\" value=\"1\">+</button></div>".PHP_EOL;
                                                else print "</div>".PHP_EOL;
                                                if ($round_win_index[$j] == $round_win_index[count($round_win_index)-1])
                                                {
                                                    print "<div class=\"bx-dark-bracket\"><span class=\"invisible d-inline-block\">Undetermined</span></div>".PHP_EOL;
                                                    print "</div>".PHP_EOL;
                                                    $divsWritten++;
                                                    if ($round_num % 2 == 0) {
                                                        print "<div class=\"bracket-space\"></div>".PHP_EOL;
                                                    }
                                                }
                                                $prevIndex = $round_win_index[$j];
                                            } elseif ($j % 2 != 0) {
                                                print "<input type=\"hidden\" name=\"seed[{$bracket[$round_win_index[$j]]['player_seed']}]\" value=\"{$bracket[$round_win_index[$j]]['player_seed']}\"/>".PHP_EOL;
                                                print "<div id=\"seed_{$bracket[$round_win_index[$j]]['player_seed']}\" class=\"bx-dark-bracket\"><span class=\"d-inline-block float-xs-left\"><div class='seed_number'>{$bracket[$round_win_index[$j]]['player_seed']}</div> {$bracket[$round_win_index[$j]]['player_name']}</span>";
                                                if ($_SESSION['user_id'] == $tournament['owner_id'] && $bracket[$round_win_index[$j]]["r{$round_num}_result"] != 1 && $bracket[$prevIndex]["r{$round_num}_result"] != 1)
                                                    print "<button class=\"float-xs-right\" type=\"submit\" name=\"".($round_num == ($round-1) ? "final" : "r{$round_num}")."_seed_win[{$bracket[$round_win_index[$j]]['player_seed']}]\" value=\"1\">+</button></div>".PHP_EOL;
                                                else print "</div>".PHP_EOL;
                                                print "</div>".PHP_EOL;
                                                $divsWritten++;
                                                if ($round_num % 2 == 0) {
                                                    print "<div class=\"bracket-space\"></div>";
                                                }
                                            }
                                        }
                                        if ($divsWritten != $divsNeeded) {
                                            do {
                                                if($divsWritten % 2 != 0) {
                                                    if ($round_num % 2 != 0) {
                                                        print "<div class=\"bracket-space\"></div>".PHP_EOL;
                                                    }
                                                    print "<div>";
                                                    print "<div class=\"bx-dark-bracket\"><span class=\"invisible d-inline-block\">Undetermined</span></div>".PHP_EOL;
                                                    print "<div class=\"bx-dark-bracket\"><span class=\"invisible d-inline-block\">Undetermined</span></div>".PHP_EOL;
                                                    print "</div>".PHP_EOL;
                                                    $divsWritten++;
                                                    if ($round_num % 2 == 0) {
                                                        print "<div class=\"bracket-space\"></div>".PHP_EOL;
                                                    }
                                                }
                                            } while($divsWritten < $divsNeeded);
                                        }
                                    } else {
                                        if ($divsWritten != $divsNeeded) {
                                            do {
                                                if ($round_num % 2 != 0) {
                                                    print "<div class=\"bracket-space\"></div>".PHP_EOL;
                                                }
                                                print "<div>";
                                                print "<div class=\"bx-dark-bracket\"><span class=\"invisible d-inline-block\">Undetermined</span></div>".PHP_EOL;
                                                print "<div class=\"bx-dark-bracket\"><span class=\"invisible d-inline-block\">Undetermined</span></div>".PHP_EOL;
                                                print "</div>".PHP_EOL;
                                                $divsWritten++;
                                                if ($round_num % 2 == 0) {
                                                    print "<div class=\"bracket-space\"></div>".PHP_EOL;
                                                }
                                            } while($divsWritten < $divsNeeded);
                                        }
                                    }
                                    if($round_num % 2 != 0) print "<div class=\"bracket-space\"></div>".PHP_EOL;
                                    print "</div>".PHP_EOL;
                                }

                            }
                            $bracket_winner = 0;
                            print "<div id=\"round_{$round}\" class=\"bracket-winner\">".PHP_EOL;
                            print "<div class=\"bx-dark text-xs-center\">Winner</div>".PHP_EOL."<div class=\"bracket-space\"></div>".PHP_EOL;
                            foreach ($bracket as $player) {
                                if ($player['bracket_win'] == 1){
                                    $bracket_winner++;
                                    break;
                                } else $bracket_winner++;
                            }
                            if ($bracket_winner != 0 && $bracket[$bracket_winner-1]['bracket_win'] == 1) {
                                print "<div id=\"seed_{$bracket[$bracket_winner-1]['player_seed']}\" class=\"bx-dark-bracket\"><span class=\"d-inline-block float-xs-left\"><div class='seed_number'>{$bracket[$bracket_winner-1]['player_seed']}</div>{$bracket[$bracket_winner-1]['player_name']}</span></div>".PHP_EOL;
                            } else print "<div class=\"bx-dark-bracket\"><span class=\"invisible d-inline-block\">Undetermined</span></div>".PHP_EOL;
                            print "<div class=\"bracket-space\"></div></div>".PHP_EOL;
                        } else {
                            print "Invalid Page Request.";
                        } ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once "modules/site.footer.php"?>