<?php
print "<h1 class=\"text-sm-center\">Tournament Creation - Naming</h1>";
print "<hr>";
print "<form id=\"create_tournament\" action=\"create.php\" method=\"post\">";
print "<label for=\"tournament_name\">Name: </label>";
print "<input style=\"float: left; width: 100%;\" type=\"text\" id=\"tournament_name\" name=\"tournament_name\"";
isset($_POST['tournament_name']) ?
    print "value=\"{$_POST['tournament_name']}\"/>"
    : print "/>";
print
    "<hr>";
print
    "<label for=\"activity_type\">Type: </label>";
print
    "<select form=\"create_tournament\" id=\"activity_type\" name=\"activity_type\">";
isset($_POST['activity_type']) ? $_POST['activity_type'] == "Game" ?
    print "<option value=\"Game\" selected>Game</option>"
    : print "<option value=\"Game\">Game</option>"
    : print "<option value=\"Game\" selected>Game</option>";
isset($_POST['activity_type']) ? $_POST['activity_type'] == "Sport" ?
    print "<option value=\"Sport\" selected>Sport</option>"
    : print "<option value=\"Sport\">Sport</option>"
    : print "<option value=\"Sport\">Sport</option>";
print "</select>";
print
    "<label for=\"activity_name\">Activity: </label>";
print "<input style=\"width: 45%;\" type=\"text\" id=\"activity_name\" name=\"activity_name\"";
isset($_POST['activity_name']) ?
    print "value=\"{$_POST['activity_name']}\"/>"
    : print "/>";
print
    "<hr>";
print
    "<label for=\"player_limit\">Number of Players:</label>"
    ."<br>"
    ."<select style=\"width: 100%\" form=\"create_tournament\" name=\"player_limit\">";
isset($_POST['player_limit']) ? $_POST['player_limit'] == 8 ?
    print "<option value=\"8\" selected>Tournament Seed of 8</option>"
    : print "<option value=\"8\">Tournament Seed of 8</option>"
    : print "<option value=\"8\" selected>Tournament Seed of 8</option>";
print
    "</select>";
print
    "<hr>";
print
    "<button class=\"btn-md-dark btn-gap float-sm-right\" type=\"submit\" name=\"cancel_creation\" value=\"Cancel Tournament Creation\">Cancel</button>";
print
    "<button class=\"btn-md-dark float-sm-right\" type=\"submit\" name=\"create_tournament\" value=\"Create Tournament\">Next</button>";
print "</form>";
?>