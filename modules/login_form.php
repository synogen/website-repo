<?php
/**
 * Tournament Brackets - Login Form
 */
?>
<form id="loginForm" method="post">
    <?php if (isset($_POST['login_create_tournament'])) print "<input type=\"hidden\" name=\"login_create_tournament\" value=\"{$_POST['login_create_tournament']}\"/>";?>
    <div class="noselect" id="formLabels">
        <label for="user">Username:</label>
        <label for="password">Password:</label>
    </div>
    <div id="formInputBoxes">
        <input type="text" id="username"
               name="username"
            <?php if(isset($_POST['username'])) print "value=\"{$_POST['username']}\"";
                  elseif (isset($_COOKIE['rememberLogin'])) print "value=\"{$_COOKIE['user']}\""; ?>
               placeholder="Enter your username...">
        <input type="password" id="password"
               name="password" <?php if (isset($_COOKIE['rememberLogin'])) print "value=\"{$_COOKIE['password']}\""; ?>
               placeholder="Enter your password...">
    </div>
    <div class="clearfix noselect" id="formRemember">
        <label for="rememberLogin"><input type="checkbox" id="rememberLogin" name="rememberLogin"
               value="rememberLogin" <?php if (isset($_COOKIE['rememberLogin'])) print "checked";?>>
        Remember Login
        </label>
    </div>
    <div class="noselect" id="formRegister"><a href="register">New User? Register Here!</a></div>
    <div id="formSubmit"><input id="btn_login" class="btn-md-dark" type="submit" name="login" value="Sign In"></div>
    <br><div class="errorDiv clearfix"><span class="error_msg"></span></div>
</form>
