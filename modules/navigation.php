<?php
/**
 * Tournament Brackets - Website Navigation Module
 * Team Project / Assignment 1
 */
?>
<!-- Sidebar -->
<div id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li class="sidebar-brand"><a href="./"><?php print $title;?></a></li>
        <li <?php if(basename($_SERVER['PHP_SELF']) == "index.php") print "class=\"current_page\"";?>><a href="./">Homepage</a></li>
        <?php if(isset($_SESSION['logged_in'])) {
            if(isset($_SESSION['user_type']))
                if ($_SESSION['user_type'] == "admin" || $_SESSION['user_type'] == "owner")
                    print "<li " . (basename($_SERVER['PHP_SELF']) == "dashboard.php" ? "class=\"current_page\"" : "") . "><a href=\"dashboard.php\">Dashboard</a></li.>";
            print "<li " . (basename($_SERVER['PHP_SELF']) == "profile.php" ? "class=\"current_page\"" : "") . "><a href=\"profile.php?id={$_SESSION['user_id']}\">Profile</a></li>";
            //print "<li" . (basename($_SERVER['PHP_SELF']) == "manage.php" ? "class=\"current_page\"" : "") . "><a href=\"manage.php\">Manage Tournaments</a></li>";
        } ?>
        <li <?php print (basename($_SERVER['PHP_SELF']) == "tournaments.php" ? "class=\"current_page\"" : "")?>><a href="tournaments.php">Tournament Listing</a></li>
        <?php if (isset($_SESSION['logged_in'])){
            if ($_SESSION['user_type'] == "owner" || $_SESSION['user_type'] == "admin")
            {
                print "<li><a href=\"/folder_view/vs.php?s=".$this_file."\" target=\"_blank\">View Source</a></li>";
            }
        }?>
        <li>
            <a
                tabindex="0"
                role="button"
                data-toggle="popover"
                data-trigger="focus"
                title="About Us"
                data-content="This website's team is composed of two members with the names of Jiacheng Lei and Jullian Sy-Lucero.
                <br/><br/>
                Our website's purpose is to provide our users with a way to create tournament brackets with ease.">
                About Us
            </a>
        </li>
    </ul>
</div>
<!-- /#sidebar-wrapper -->
<!-- Top Navigation Bar -->
<nav id="top-navbar" class="navbar navbar-default navbar-fixed-top navbar-style toggled">
    <div id="sidebar-button"><a class="btn btn-default" id="menu-toggle" tabindex="0">►</a></div>
    <a class="navbar-brand font-weight-bold" href="./"><?php print $title;?></a>
    <div id="account_dropdown" class="btn-group">
        <?php if (basename($_SERVER['PHP_SELF']) == "login.php")
            print "<form action=\"./\"><button type=\"submit\" class=\"btn btn-secondary\">Return to Home</button></form>";
        else if(basename($_SERVER['PHP_SELF']) == "register.php")
            if(isset($_SESSION['logged_in'])) print "<form action=\"./\"><button type=\"submit\" class=\"btn btn-secondary\">Return to Home</button></form>";
            else print "<form action=\"login\"><button type=\"submit\" class=\"btn btn-secondary\">Return to Sign In</button></form>";
        elseif (isset($_SESSION['user_id']))
            print "<form id=\"profile_button\" method=\"get\" action=\"profile\" class=\"btn-group\">"
                    ."<button type=\"submit\" class=\"btn btn-sm btn-secondary\" name=\"id\" value=\"".$_SESSION['user_id']."\">".$database_access->GetUsername($_SESSION['user_id'])."</button>"
                    ."<button type=\"button\" class=\"btn btn-secondary dropdown-toggle dropdown-toggle-split\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">"
                        ."<span class=\"sr-only\">Toggle Dropdown</span>"
                    ."</button>"
                ."</form>"
                //."<button type=\"button\" class=\"btn btn-secondary dropdown-toggle dropdown-toggle-split\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">"
                //    ."<span class=\"sr-only\">Toggle Dropdown</span>"
                //."</button>"
                ."<div class=\"dropdown-menu dropdown-menu-right\">"
                    //."<button class=\"dropdown-item\" type=\"button\">Profile Settings</button>"
                    //."<button class=\"dropdown-item\" type=\"button\">Settings</button>"
                    ."<form method=\"post\" action=\"login.php\">"
                        ."<button id=\"logout\" class=\"dropdown-item\" type=\"submit\" name=\"logout\" value=\"Log Out\">Log Out</button>"
                    ."</form>"
                ."</div>";
        else print "<form action=\"login\"><button type=\"submit\" class=\"btn btn-secondary\">Sign In</button></form>"?>
    </div>
</nav>
<!-- /#top_nav -->
