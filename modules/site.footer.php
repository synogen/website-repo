<?php
/**
 * Tournament Brackets - Site Footer
 */
?>
<!-- jQuery -->
<script src="javascript/jquery-3.1.1.js"></script>
<!-- jQuery Hotkeys -->
<script src="javascript/jquery.hotkeys.js"></script>
<!-- Tether IO JavaScript Required By Bootstrap v4 -->
<script src="javascript/tether.min.js"></script>
<!-- Bootstrap 4 Core JavaScript -->
<script src="javascript/bootstrap.min.js"></script>
<!-- Menu Toggle Script -->
<script type="text/javascript" src="javascript/menu_actions.js"></script>
</body>
</html>
