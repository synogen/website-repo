<?php
/**
 * Tournament Brackets - Redirect Module
 */
$timer = strtotime("+10 second");
$currTime = time();
$timeDiff = ($timer - $currTime);
if (isset($redirect_url)) header("Refresh: 5; URL={$redirect_url}");
else header("Refresh: 5; URL=login");
?>
<div style="margin:20% auto; text-align: center;">
    <p><span id="">This page is currently viewable to members only.</span>
        <br>You are not logged in to view this page.
        <br>You will be redirected in <span id="timer">5</span> seconds.</p>
</div>
<script>
    function startTimer(duration, display) {
        var timer = duration, minutes, seconds;
        setInterval(function () {
            if (timer >= 60) minutes = parseInt(timer / 60, 10);
            seconds = parseInt(timer % 60, 10);

            if (timer >= 60)seconds = seconds < 10 ? "0" + seconds : seconds;

            display.textContent = timer >= 60 ? minutes + ":" + seconds : seconds;

            if (--timer < 0) {
                timer = duration;
            }
        }, 1000);
    }

    window.onload = function () {
        var timeInSeconds = 4,
            display = document.querySelector('#timer');
        startTimer(timeInSeconds, display);
    };
</script>
