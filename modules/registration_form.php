<?php
/**
 * Tournament Brackerts - Registration Form
 */
?>
<div class="row">
    <div id="register_form" class="col-xs-12 bx-dark">
        <form method="post">
            <h2>Site Registration</h2>
            <hr>
            <div id="form_sections" class="form-group">
                <div id="formName">
                    <div id="formFirstName">
                        <label class="noselect" for="first_name">First Name: </label>
                        <br><input type="text" id="last_name" name="first_name" <?php isset($_POST['first_name']) ? print "value=\"{$_POST['first_name']}\" " : print "";?>placeholder="Enter your first name...">
                    </div>
                    <div id="formLastName">
                        <label class="noselect" for="last_name">Last Name: </label>
                        <br><input type="text" id="last_name" name="last_name" <?php isset($_POST['last_name']) ? print "value=\"{$_POST['last_name']}\" " : print "";?>placeholder="Enter your last name...">
                    </div>
                </div>
                <br class="clearfix"/><br/><hr/>
                <div id="formUserInfo">
                    <div id="formUsername">
                        <label class="noselect" for="username">Username: </label>
                        <br><input type="text" id="username" name="username" <?php isset($_POST['username']) ? print "value=\"{$_POST['username']}\" " : print "";?>placeholder="Enter your username...">
                    </div>
                    <div id="formEmail">
                        <div id="email_section">
                            <label class="noselect" for="email">Email: </label>
                            <br><input type="email" id="email" name="email" <?php isset($_POST['email']) ? print "value=\"{$_POST['email']}\" " : print "";?>placeholder="Enter you email..">
                        </div>
                    </div>
                    <div id="formPassword" class="clearfix">
                        <div id="password_section">
                            <label class="noselect" for="password">Password: </label>
                            <br><input type="password" id="password" name="password" <?php isset($_POST['password']) ? print "value=\"{$_POST['password']}\" " : print "";?>placeholder="Enter your password...">
                        </div>
                        <div id="confirm_password_section">
                            <label class="noselect" for="confirm_password">Confirm Password: </label>
                            <br><input type="password" id="confirm_password" name="confirm_password" <?php isset($_POST['confirm_password']) ? print "value=\"{$_POST['confirm_password']}\" " : print "";?>placeholder="Confirm password..."/>
                        </div>
                    </div>
                </div>
                <br class="clearfix"/><br/><br/><hr/>
                <div id="formButtons">
                    <?php if(isset($_SESSION['logged_in'])) print "<button id=\"btn_returnToHome\" class=\"btn-lg-dark\">Return to Home</button>";
                            else print "<button id=\"btn_returnToLogin\" class=\"btn-lg-dark\">Return to Login</button>";?>
                    <input id="register" class="btn-lg-dark" type="submit" name="register" value="Register">
                </div>
            </div>
        </form>
    </div>
</div>
<?php
print "<script type=\"text/javascript\">";
if(isset($_SESSION['logged_in'])) print "document.getElementById(\"btn_returnToHome\").onclick = function (e) {
        e.preventDefault();
        window.location.href = \"index\";
    };";
else print "document.getElementById(\"btn_returnToLogin\").onclick = function (e) {
        e.preventDefault();
        window.location.href = \"login\";
    };";
print "</script>";
?>
