<?php
//Storing Tournament Creation Form Information
$tournament_name =
    isset($_POST['tournament_name']) && !empty($_POST['tournament_name']) ?
        $_POST['tournament_name'] : "Tournament";
$activity_type =
    isset($_POST['activity_type']) && !empty($_POST['activity_type']) ?
        $_POST['activity_type'] : "Game";
$activity_name =
    isset($_POST['activity_name']) && !empty($_POST['activity_name']) ?
        $_POST['activity_name'] : "Unknown";
$player_limit =
    isset($_POST['player_limit']) && !empty($_POST['player_limit']) ?
        $_POST['player_limit'] : 8;

//Writing Bracket Creation Form.
print
    "<h1 class=\"text-sm-center\">Tournament Creation - Bracket</h1>";
print
    "<hr>";
print
    "<div style=\"text-align: center;\"><form action=\"create.php\" method=\"post\">";
//Creating Hidden Input Elements To Store Tournament Information Forms.
print
    "<input type=\"hidden\" name=\"tournament_name\" value=\"{$tournament_name}\"/>"
    ."<input type=\"hidden\" name=\"activity_type\" value=\"{$activity_type}\"/>"
    ."<input type=\"hidden\" name=\"activity_name\" value=\"{$activity_name}\"/>"
    ."<input type=\"hidden\" name=\"player_limit\" value=\"{$player_limit}\"/>";
for ($i = 0; $i < $player_limit; $i++)
{
    $player_seed_num = $i + 1;
    print
        "<input type=\"hidden\" name=\"seed[{$i}]\" value=\"$player_seed_num\"/>"
        ."<input style=\"width: 75%; text-align: left; box-shadow: 0 2px 5px rgba(0,0,0,0.5);\" type=\"text\" name=\"players[{$i}]\" placeholder=\"Player @ Seed #{$player_seed_num}\"";
    isset($_POST['players'][$i]) && !empty($_POST['players'][$i])?
        print "value=\"{$_POST['players'][$i]}\"/>"
        : print "/>";
    print
        "<hr>";
}
print
    "<button class=\"btn-lg-dark btn-gap float-sm-right\" type=\"submit\" name=\"cancel_creation\" value=\"Cancel Tournament Creation\"
    onsubmit=\"window.onbeforeunload = function(){ return 'Are you sure you want to leave?'; };\">Cancel Creation</button>";
print
    "<button class=\"btn-lg-dark float-sm-right\" type=\"submit\" name='create_bracket' value=\"Create Bracket\">Add Players</button>";
print
    "</form></div>";
?>