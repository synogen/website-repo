<?php
/**
 * Created by PhpStorm.
 * User: Jullian
 * Date: 2016-12-05
 * Time: 10:44 AM
 */
session_start();
$title = "Versus";
require_once "frameworks/Medoo/medoo.php";
require_once "classes/database.access.php";
$client_ip = $_SERVER['REMOTE_ADDR'];
$database_access = new Access();
$error_msg = "";
if (isset($_SESSION['logged_in'])) {
    if (isset($_POST['logout'])) {
        session_unset();
    }
}