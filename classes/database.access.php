<?php
class Access {
    //Private Variables
    public $database;
    private $current_username;
    private $insertId;

    //Database Access Constructor
    public function __construct() {
        $this->database = new \medoo([
        'database_type' => 'mysql',
        'database_name' => 'f6team33_database',
        'server' => 'localhost',
        'username' => 'f6team33_admin',
        'password' => 'dbaccess',
        'charset' => 'utf8']);
    }

    public function GetLastInsertID() {
        return $this->insertId;
    }

    public function GetTable($table_name, $where = null) {
        if(isset($where))
            return $this->database->select($table_name, "*", $where);
        else
            return $this->database->select($table_name, "*");
    }

    public function GetTableLastRow($table_name, $where = null)
    {
        $table = isset($where) ? $this->GetTable($table_name, $where) : $this->GetTable($table_name);
        return $table[count($table)-1];
    }

    public function RenderUsersTable($users) {
        print
            "<div class=\"table-responsive\">"
                ."<form id=\"accounts_dashboard\" action=\"{$_SERVER['PHP_SELF']}\" method='post'>"
                ."<table class=\"table table-bordered table-striped table-hover\">"
                    ."<thead class=\"thead-inverse\">"
                        ."<tr>"
                            ."<th>ID</th>"
                            ."<th>Type</th>"
                            ."<th>User</th>"
                            ."<th>Email</th>"
                            ."<th>Date Created</th>"
                            ."<th>Options</th>"
                        ."</tr>"
                    ."</thead>"
                    ."<tbody>";
        foreach ($users as $user) {
            print
                "<tr>"
                . "<td><input type=\"hidden\" name=\"user_id[{$user['user_id']}]\" value=\"{$user['user_id']}\">{$user['user_id']}</td>"
                . "<td><input type=\"hidden\" name=\"original_user_type[{$user['user_id']}]\" value=\"{$user['type']}\"/>";
            isset($_SESSION['user_type']) && isset($_SESSION['user']) ?
                $_SESSION['user_type'] != $user['type'] || $_SESSION['user'] == $user['username'] ?
                    print "<select form=\"accounts_dashboard\" name=\"user_type[{$user['user_id']}]\">"
                              ."<option value=\"user\" ".($user['type'] == "user" ? "selected" : "").">User</option>"
                              .($_SESSION['user_type'] == "owner" ? "<option value=\"admin\"".($user['type'] == "admin" ? "selected" : "").">Admin</option>" : "")
                              .($_SESSION['user_type'] == "owner" ? "<option value=\"owner\"".($user['type'] == "owner" ? "selected" : "").">Owner</option>" : "")
                          ."</select>"
                    : print "<input type=\"hidden\" name=\"user_type[{$user['user_id']}]\" value=\"{$user['type']}\"/>{$user['type']}</td>"
                    : print "<input type=\"hidden\" name=\"user_type[{$user['user_id']}]\" value=\"{$user['type']}\"/>{$user['type']}</td>";
            print "<td><input type=\"hidden\" name=\"original_username[{$user['user_id']}]\" value=\"{$user['username']}\"/>";
            isset($_SESSION['user_type']) && isset($_SESSION['user']) ?
                $_SESSION['user_id'] == $user['user_id'] || $_SESSION['user_type'] != $user['type'] || $_SESSION['user_type'] == "owner" ?
                    print "<input type=\"text\" name=\"username[{$user['user_id']}]\" value=\"{$user['username']}\"/></td>"
                    : print "<input type=\"hidden\" name=\"username[{$user['user_id']}]\" value=\"{$user['username']}\"/>{$user['username']}</td>"
                : print "<input type=\"hidden\" name=\"username[{$user['user_id']}]\" value=\"{$user['username']}\"/>{$user['username']}</td>";
            print "<td>{$user['email']}</td>"
                . "<td>{$user['date_created']}</td>";
            isset($_SESSION['user_type']) && isset($_SESSION['user']) ?
                $_SESSION['user_type'] != $user['type'] || $_SESSION['user_id'] == $user['user_id'] ?
                    print "<td>"
                        ."<button type=\"submit\" class=\"btn-md-dark btn-gap\" name=\"change_user_info\" value=\"Change Information\">Change</button>"
                        ."<button type=\"submit\" class=\"btn-md-dark btn-gap\" name=\"delete_user[{$user['user_id']}]\" value=\"Delete User\">Delete</button>"
                        ."</td>"
                    : print "<td>No Options Available.</td>"
                : print "<td>No Options Available.</td>";
        }
        print
                    "</tbody>"
                ."</table>"
                ."</form>"
            ."</div>";
    }

    public function RenderTournamentsTable($tournaments) {
        print
            "<div class=\"table-responsive\">"
            ."<table class=\"table table-bordered table-striped table-hover\">"
            ."<thead class=\"thead-inverse\">"
            ."<tr>"
            ."<th>ID</th>"
            ."<th>Tournament Name</th>"
            ."<th>Type</th>"
            ."<th>Activity</th>"
            ."<th>Owner</th>"
            ."<th>Status</th>"
            ."<th>Link</th>"
            ."</tr>"
            ."</thead>"
            ."<tbody>";
        if (count($tournaments) > 0) {
            foreach ($tournaments as $tournament) {
                print
                    "<tr>"
                    . "<td>{$tournament['tournament_id']}</td>"
                    . "<td>{$tournament['tournament_name']}</td>"
                    . "<td>{$tournament['activity_type']}</td>"
                    . "<td>{$tournament['activity_name']}</td>"
                    . "<td>{$this->GetUsername($tournament['owner_id'])}</td>"
                    . "<td>".($this->IsCompletedTournament($tournament['tournament_id']) ? "Complete" : "Incomplete" )."</td>"
                    . "<td><form action=\"bracket.php\" method=\"get\"><button type=\"submit\" class=\"btn-md-dark\" name=\"id\" value=\"{$tournament['tournament_id']}\">View</button></form></td>"
                    . "</tr>";
            }
        }
        else { print "<td colspan=\"7\">Currently no tournaments have been created.</td>"; }
        print
            "</tbody>"
            ."</table>"
            ."</div>";
    }

    //Insert a logged message to the logs table
    //Method uses an insert clause to store log messages to the database
    //The insert string query would look like this:
    //INSERT INTO logs (message)
    //VALUES ($msg);
    public function Log($client_ip, $msg)
    {
        $this->database->insert("logs", [
            "client_ip" => $client_ip,
            "message" => $msg,
        ]);
    }

    //Insert a new user to the database by passing Username, Password, First Name, Last Name and Email
    //Method uses an insert clause to store new user to the database
    //The insert string query would look like this:
    //INSERT INTO users (username, password, first_name, last_name, email)
    //VALUES ($username, $password, $first_name, $last_name, $email);
    public function InsertNewUser($username, $password, $first_name, $last_name, $email, $user_type = "user")
    {
        $this->database->insert("users", [
            "type" => $user_type,
            "username" => $username,
            "password" => $password,
            "first_name" => $first_name,
            "last_name" => $last_name,
            "email" => $email,
        ]);
    }

    public function GetUserTypeWithUsername($username) {
        return $this->database->get("users", "type", ["username" => $username]);
    }

    public function GetUserTypeWithUserID($id) {
        return $this->database->get("users", "type", ["user_id" => $id]);
    }

    //Get Username by passing a User ID
    //Method uses a get clause to get value in the column of username
    //@ table of "users" where user_id is equal to the User ID passed.
    public function GetUsername($user_id)
    {
        return $this->database->get("users", "username", ["user_id" => $user_id]);
    }

    //Get Proper Username by passing a Username
    //Method uses a get clause to get value in the column of username
    //@ table of "users" where user_id is equal to the Username passed.
    public function GetProperUsername($username)
    {
        return $this->database->get("users", "username", ["username" => $username]);
    }

    //Get User ID by passing a Username
    //Method uses a get clause to get value in the column of user_id
    //@ table of "users" where username is equal to the username passed.
    public function GetUserID($username)
    {
        return $this->database->get("users", "user_id", ["username" => $username]);
    }

    //Get User ID by passing a Username
    //Method uses a get clause to get value in the column of first_name
    //@ table of "users" where username is equal to the username passed.
    public function GetUserFirstName($username)
    {
        return $this->database->get("users", "first_name", ["username" => $username]);
    }

    //Get User ID by passing a Username
    //Method uses a get clause to get value in the column of first_name
    //@ table of "users" where username is equal to the username passed.
    public function GetUserFirstNameWithID($id)
    {
        return $this->database->get("users", "first_name", ["user_id" => $id]);
    }

    //Get User ID by passing a Username
    //Method uses a get clause to get value in the column of last_name
    //@ table of "users" where username is equal to the username passed.
    public function GetUserLastName($username)
    {
        return $this->database->get("users", "last_name", ["username" => $username]);
    }

    //Get User ID by passing a Username
    //Method uses a get clause to get value in the column of last_name
    //@ table of "users" where username is equal to the username passed.
    public function GetUserLastNameWithID($id)
    {
        return $this->database->get("users", "last_name", ["user_id" => $id]);
    }

    //Get User ID by passing a Username
    //Method uses a get clause to get value in the column of email
    //@ table of "users" where username is equal to the username passed.
    public function GetUserEmail($username)
    {
        return $this->database->get("users", "email", ["username" => $username]);
    }

    //Get User ID by passing a Username
    //Method uses a get clause to get value in the column of email
    //@ table of "users" where username is equal to the username passed.
    public function GetUserEmailWithID($id)
    {
        return $this->database->get("users", "email", ["user_id" => $id]);
    }

    //Get Current Username
    //Method returns the currentUser variable holding the user's username.
    public function GetCurrentUsername()
    {
        return $this->current_username;
    }

    //Set Current Username by passing a Username from a successful login.
    //Method sets the current username uses a get clause to get value in the
    //column of username @ table of "users" where username is equal to the
    //currently logged in user's username.
    public function SetCurrentUsername($username)
    {
        $this->current_username = $this->database->get("users", "username", ["username" => $username]);
    }

    //Get User Profile Information by passing a Username
    //Method uses a get clause to get value in each column @ table of "users"
    //where username is equal to the username passed.
    public function GetUserProfile($username)
    {
        return $this->database->get("users", [
            "user_id",
            "username",
            "first_name",
            "last_name",
            "email",
        ], [
            "username" => $username
        ]);
    }

    public function GetUserProfileWithUserID($id)
    {
        return $this->database->get("users", [
            "user_id",
            "username",
            "first_name",
            "last_name",
            "email",
        ], [
            "user_id" => $id
        ]);
    }

    //Get User Profile Information by passing a User ID
    //Method uses a get clause to get value in each column @ table of "users"
    //where user_id is equal to the User ID passed.
    public function GetIDProfile($user_id)
    {
        return $this->database->get("users", [
            "user_id",
            "username",
            "first_name",
            "last_name",
            "email",
        ], [
            "user_id" => $user_id
        ]);
    }

    //Checks User Existence
    public function IsExistingUser($username)
    {
        return $this->database->has("users", ["username" => $username]);
    }

    public function IsRegisteredEmail($email)
    {
        return $this->database->has("users", ["email" => $email]);
    }

    //Checks For Matching Password
    public function IsValidUserLogin($username, $password)
    {
        if($this->IsExistingUser($username) || $this->IsRegisteredEmail($username)) {
            return $this->database->has("users",
                [
                    "password" => $password,
                ]);
        }
        else return false;
    }

    public function CreateTournamentTable($tournament_name, $activity_type, $activity_name, $player_limit, $owner_id)
    {
        $this->insertId = $this->database->insert("tournaments",
            [
                "owner_id" => $owner_id,
                "tournament_name" => $tournament_name,
                "activity_type" => $activity_type,
                "activity_name" => $activity_name,
                "player_limit" => $player_limit,
            ]);
    }

    public function CreateTournamentBracket($tournament_id, $players, $seed = [0 => 1, 1 => 2, 2 => 3, 3 => 4, 4 => 5, 5 => 6, 6 => 7, 7 => 8])
    {
        $player_seed_index = 0;
        foreach ($players as $player) {
            $this->database->insert("tournament_brackets", ["tournament_id" => $tournament_id, "player_seed" => $seed[$player_seed_index], "player_name" => $player]);
            $player_seed_index++;
        }
    }

    public function GetTournamentID($name) {
        return $this->database->get("tournaments", "tournament_id", ["tournament_name" => $name]);
    }

    public function GetTournamentName($id)
    {
        return $this->database->get("tournaments", "tournament_name", ["tournament_id" => $id]);
    }

    public function UpdatePlayerRoundWinResult($id, $seed, $round, $result)
    {
        $this->database->update("tournament_brackets", ["r{$round}_result" => $result],  ["AND" => ["tournament_id" => $id, "player_seed" => $seed]]);
    }

    public function UpdatePlayerBracketWinResult($id, $seed, $result)
    {
        $this->database->update("tournament_brackets", ["bracket_win" => $result],  ["AND" => ["tournament_id" => $id, "player_seed" => $seed]]);
    }

    public function IsCompletedTournament($tournament_id) {
        $bracket = $this->GetTable("tournament_brackets", ["tournament_id" => $tournament_id]);
        foreach ($bracket as $player) {
            if ($player['bracket_win'] == 1) return true;
        }
        return false;
    }

    public function CreatePost($post_type, $post, $creator_id)
    {
        $this->database->insert("posts", ["post_type" => $post_type, "post" => $post, "creator_id" => $creator_id]);
    }

    public function exec($query)
    {
        $this->database->exec($query);
    }
}