<?php
/**
 * <Site Name> - <What this is>
 */
include_once "modules/site.variables.php";
include_once "modules/site.header.php";
$this_file = __FILE__;
$subtitle = "Page Name";
?>
    <title><?php print "{$title} - {$subtitle}";?></title>
    </head>
    <body>
<div id="wrapper" class="toggled">
    <?php include_once "modules/navigation.php";?>
    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 content-bx bx-dark text-sm-left">
                    <h1>All Tournaments Listing</h1>
                    <?php $database_access->RenderTournamentsTable($database_access->GetTable("tournaments"));?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once "modules/site.footer.php"?>