<?php
/**
 * Tournament Brackets - Website Login Page
 * Team Project / Assignment 1
 */
include_once "modules/site.variables.php";
include_once "./modules/site.header.php";
$subtitle = "Registration";
$home_url = "./";
$this_file = __FILE__;
?>
    <link rel="stylesheet" type="text/css" href="./styles/register.css"/>
    <title><?php print "{$title} - {$subtitle}"?></title>
</head>
<body>
<div id="wrapper" class="toggled">
    <?php include_once "./modules/navigation.php";?>
    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-10 offset-xs-1">
                    <?php
                    if(isset($_POST['register']))
                    {
                        $type = "user";
                        $username = isset($_POST['username']) ? $_POST['username'] : "";
                        $password = isset($_POST['password']) ? $_POST['password'] : "";
                        $confirm_password = isset($_POST['confirm_password']) ? $_POST['confirm_password'] : "";
                        $first_name = isset($_POST['first_name']) ? $_POST['first_name'] : "";
                        $last_name = isset($_POST['last_name']) ? $_POST['last_name'] : "";
                        $email = isset($_POST['email']) ? $_POST['email'] : "";
                        if (!empty($type&$username&$password&$confirm_password&$first_name&$last_name&$email))
                        {
                            if (!$database_access->IsExistingUser($username)) {
                                if(!$database_access->IsRegisteredEmail($email)) {
                                    if ($password == $confirm_password) {
                                        $database_access->InsertNewUser($username, $password, $first_name, $last_name, $email);
                                        $database_access->Log($client_ip, "New User ({$username}/{$email}) has registered.")
                                        ?>
                                        <div class="row">
                                            <div class="col-xs-6 offset-xs-3 content-bx bx-dark text-sm-center">
                                                <form action="login.php">
                                                    <h4>Registration Completed!</h4>
                                                    <button class="btn-lg-dark" type="submit">Go to Sign In</button>
                                                </form>
                                            </div>
                                        </div>
                                        <?php
                                    } else {
                                        $error_msg = "Password confirmation did not match the password first given.";
                                    }
                                } else {
                                    $error_msg = "An account is already registered with {$email}.";
                                }
                            } else {
                                $error_msg = "Username is already in use. Please choose another username.";
                            }
                        } else {
                            $error_msg = "Please enter all required information.";
                        }
                    }
                    if (!isset($_POST['register']) || !empty($error_msg))
                    {
                        if(!empty($error_msg))
                            print "<div class=\"col-xs-6 offset-xs-3 alert alert-danger alert-dismissible fade in\" role=\"alert\">"
                            ."<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">"
                            ."<span aria-hidden=\"true\">&times;</span>"
                            ."</button>"
                            ."<span class=\"font-weight-bold\">{$error_msg}</span>"
                            ."</div>";
                        include_once "./modules/registration_form.php";
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
<?php include_once "./modules/site.footer.php"; ?>
