<?php
/**
 * Tournament Brackets - Website Login Page
 * Team Project / Assignment 1
 */
include_once "modules/site.variables.php";
$subtitle = "Login";
$home_url = "/";
$this_file = __FILE__;
if (isset($_SESSION['logged_in'])) {
    header("Location: {$home_url}");
}
$username = isset($_POST['login']) ? $_POST['username'] : "";
$password = isset($_POST['login']) ? $_POST['password'] : "";
if (isset($_POST['login']) && !empty($username&$password)) {
    if ($database_access->IsExistingUser($username)) {
        if ($database_access->IsValidUserLogin($username, $password)) {
            if (isset($_POST['rememberLogin'])) {
                setcookie("rememberLogin", $_POST['rememberLogin'], strtotime("+3 month"));
                setcookie("user", $username, strtotime("+3 month"));
                setcookie("password", $password, strtotime("+3 month"));
            } else {
                setcookie("rememberLogin", "", strtotime("January 1, 0001"));
                setcookie("user", "", strtotime("January 1, 0001"));
                setcookie("password", "", strtotime("January 1, 0001"));
            }
            $database_access->SetCurrentUsername($username);
            $_SESSION['logged_in'] = true;
            $_SESSION['user'] = $database_access->GetCurrentUsername();
            $_SESSION['user_id'] = $database_access->GetUserID($_SESSION['user']);
            $_SESSION['user_type'] = $database_access->GetUserTypeWithUserID($_SESSION['user_id']);
            $database_access->Log($client_ip, "Successful Login as {$_SESSION['user']}");
            if(isset($_POST['login_create_tournament'])) header("Location: create.php");
            else header("Location: {$home_url}");
        } else {
            $error_msg = "Invalid Password was entered for {$database_access->GetProperUsername($username)}";
            $database_access->Log($client_ip, $error_msg);
        }
    }
    else
    {
        $error_msg = "Invalid Username was entered. Did you make a mistake?";
    }
}
include_once "./modules/site.header.php";
?>
    <link rel="stylesheet" type="text/css" href="./styles/login.css"/>
    <title><?php print "{$title} - {$subtitle}" ?></title>
</head>
<body>
<div id="wrapper" class="toggled">
    <?php include_once "./modules/navigation.php";?>
    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <?php
                    if(isset($_POST['login']) && empty($username&$password)) $error_msg = "Please do not leave any empty fields";
                    if(!empty($error_msg))
                        print "<div class=\"col-xs-6 offset-xs-3 alert alert-danger alert-dismissible fade in\" role=\"alert\">"
                                  ."<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">"
                                      ."<span aria-hidden=\"true\">&times;</span>"
                                  ."</button>"
                                  ."<span class=\"font-weight-bold\">{$error_msg}</span>"
                              ."</div>";
                    include_once "./modules/login_form.php";
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
<?php include_once "./modules/site.footer.php"; ?>