<?php
/**
 * Tournament Brackets - Website Home/Landing Page
 * Team Project / Assignment 1
 */
include_once "modules/site.variables.php";
include_once "./modules/site.header.php";
$subtitle = "Home";
$this_file = __FILE__;
if (isset($_POST['post_update']))
{
    if (!empty($_POST['post']))
    {
        $database_access->CreatePost($_POST['post_type'], $_POST['post'], $_POST['user_id']);
    }
}
?>
    <title><?php print "{$title} - {$subtitle}"; ?></title>
</head>
<body>
<div id="wrapper" class="toggled">
    <?php include_once "./modules/navigation.php";?>
    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-8 content-bx bx-dark text-sm-left">
                    <h3 class="noselect">Latest Updates</h3>
                    <hr>
                        <?php if (isset($_SESSION['logged_in'])) {
                            if (isset($_SESSION['user_type']))
                                if($_SESSION['user_type'] == "admin" || $_SESSION['user_type'] == "owner") { ?>
                                    <form id="posts" action="index.php" method="post">
                                        <input type="hidden" name="post_type" value="update"/>
                                        <input type="hidden" name="user_id" value="<?php print (isset($_SESSION['user']));?>"/>
                                        <textarea style="width: 100%;" maxlength="10000" form="posts" name="post" placeholder="Write a site update..."></textarea>
                                        <br><button class="btn-lg-dark float-sm-right" type="submit" name="post_update" value="post update">Post Update</button>
                                    </form>
                                    <br/>
                                    <br/>
                                    <hr class="clearfix"/>
                               <?php }
                        }
                        if (count($database_access->GetTable("posts", ["post_type" => "update"])) > 0) {
                            $latest_update_post = $database_access->GetTableLastRow("posts", ["post_type" => "update"]);
                            print
                                "<div class=\"posts\">"
                                ."<p>";
                            print
                                "{$latest_update_post['post']}"
                                ."</p>";
                            print "<hr/><div class='float-xs-right'>";
                            print
                                "<span class=\"text-muted-custom\">".date('F j, Y \<\s\p\a\n \c\l\a\s\s\=\"\t\e\x\t\-\m\u\t\e\d\"\>\@ g:i A', strtotime($latest_update_post['time_created']))."</span>"
                                ."<br/><span class=\"float-xs-right text-muted\">"
                                ."— {$database_access->GetUserLastNameWithID($latest_update_post['creator_id'])}, "
                                ."{$database_access->GetUserFirstNameWithID($latest_update_post['creator_id'])}"
                                ."</span>";
                            print "</div><br class=\"clearfix\"></div>";
                        } else {
                            print "<div class=\"posts\"><p>No posts have been created yet.<br>But don't worry, we'll have you updated soon!</p></div>";
                        }?>
                </div>
                <div class="col-xs-4 col-fixed">
                    <div class="row">
                        <div id="welcome_box" class="col-xs-12 content-bx bx-dark text-sm-left">
                            <?php if(isset($_SESSION['logged_in'])) {
                                print "<h4><spam class=\"font-weight-normal text-muted-custom\">Welcome</spam> {$_SESSION['user']}<span style=\"\">!</span></h4>";
                                ?>
                            <?php } else {
                                print "<h4><a href=\"login\">Sign in</a> to start creating brackets!</h4>";
                                ?>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row">
                        <div id="tournament_create_box" class="col-xs-12 content-bx bx-dark text-sm-center">
                            <form <?php if(isset($_SESSION['logged_in'])) print "action=\"create.php\""; else { print "action=\"login.php\" method=\"post\""; }?>>
                            <button class="btn-lg-dark" type="submit" <?php if(!isset($_SESSION['logged_in'])) print "name=\"login_create_tournament\" value=\"send to tournament creation\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Clicking this will take you to the sign in. To start creating you must be signed in.\""; ?>>Create a Tournament</button>
                            </form>
                            <hr>
                            <p>
                                To begin creating a bracket
                                <br>Press the button above.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
<?php include_once "./modules/site.footer.php"; ?>