$(function () {
    $('[data-toggle="tooltip"]').tooltip({html: true});
});

$(function () {
    $('[data-toggle="popover"]').popover({html: true})
});

$('.popover-dismiss').popover({
    trigger: 'focus'
})

$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
    this.text == "►" ? this.text = "◄" : this.text = "►"; //◄ ►
});

$(document).bind('keydown', 'ctrl+right', function(e){
    e.preventDefault();
    $("#wrapper").removeClass("toggled");
    $('#menu-toggle').text("◄"); //◄ ►
});

$('#account_dropdown').on('show.bs.dropdown', function(e){
    $(this).find('.dropdown-menu').first().stop(true, true).slideDown(150,"swing");
});

$('#account_dropdown').on('hide.bs.dropdown', function(e){
    $(this).find('.dropdown-menu').first().stop(true, true).slideUp(150,"swing");
});

//Non-Element Functions
$(document).bind('keydown', 'ctrl+left', function(e){
    e.preventDefault();
    $("#wrapper").addClass("toggled");
    $('#menu-toggle').text("►");
});

//Scroll Functions
var opacity = 1;
var shadowOpacity = 1;
var lastScrollTop = 0;
$(document).scroll(function(){
    var st = $(this).scrollTop();
    if(st == 0) {
        $('.navbar-style').css('background', 'rgba(100, 79, 140,1.00)');
        $('.navbar-style').css('b', '0 0 15px rgba(0,0,0,1.0);');
    }

    if(st > 50) {
        if (opacity > 0.37 && (st > lastScrollTop)) {
            $('.navbar-style').css('background', 'rgba(100, 79, 140,'+(opacity-0.03)+")");
            opacity -= 0.03;
            shadowOpacity -= 0.11;
        }
        else if (opacity < 1 && (st < 350)) {
            $('.navbar-style').css('background', 'rgba(100, 79, 140,'+(opacity+0.03)+")");
            opacity += 0.03;
            shadowOpacity += 0.1;
        }
    }
    lastScrollTop = st;
});