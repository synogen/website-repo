<?php
/**
 * Tournament Brackets - Create Tournament
 */
include_once "modules/site.variables.php";
$this_file = __FILE__;
if (isset($_POST['cancel_creation']))
{
    header("Location: /");
}
include_once "modules/site.header.php";
$subtitle = "Tournament Creation";
?>
    <title><?php print "{$title} - {$subtitle}";?></title>
    </head>
    <body>
<div id="wrapper" class="toggled">
    <?php include_once "modules/navigation.php";?>
    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-8 offset-xs-2 content-bx bx-dark text-sm-left">
                    <?php if (isset($_SESSION['logged_in'])) {
                            if (isset($_POST['create_tournament'])) {
                                if (empty($_POST['tournament_name']) || empty($_POST['activity_name']))
                                {
                                    $error_msg = "Please enter all details before continuing.";
                                    print "<p class=\"error_msg\">{$error_msg}</p>";
                                    include_once "modules/create_naming.php";
                                } else {
                                    include_once "modules/create_bracket.php";
                                }
                                ?>
                    <?php } elseif (isset($_POST['create_bracket'])) {
                                if (array_search("", $_POST['players']) !== false)
                                {
                                    $error_msg = "Please enter a name for all players before continuing.";
                                    print "<p class=\"error_msg\">{$error_msg}</p>";
                                    include_once "modules/create_bracket.php";
                                } else {
                                    $database_access->CreateTournamentTable($_POST['tournament_name'], $_POST['activity_type'], $_POST['activity_name'], $_POST['player_limit'], $_SESSION['user_id']);
                                    $database_access->CreateTournamentBracket($database_access->GetTable("tournaments")[count($database_access->GetTable("tournaments"))-1]['tournament_id'], $_POST['players'], $_POST['seed']);
                                    print
                                        "<div class=\"row\">"
                                            ."<form action=\"/\">"
                                                ."<h4>Tournament Created!</h4>"
                                                ."<button class=\"btn-lg-dark\" type=\"submit\">Return to Homepage</button>"
                                            ."</form>"
                                        ."</div>";
                                }
                            } else {
                                include_once "modules/create_naming.php";
                        }
                    } else { include_once "modules/redirect.php"; }?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once "modules/site.footer.php"?>