<?php
/**
 * Tournament Brackets - User Profile Page
 * Team Project / Assignment 1
 */
include_once "modules/site.variables.php";
include_once "./modules/site.header.php";
$this_file = __FILE__;
if (isset($_GET['user']))
    $subtitle = !empty($database_access->GetProperUsername($_GET['user'])) ? $database_access->GetProperUsername($_GET['user']) : "User Not Found.";
elseif (isset($_GET['id']))
    $subtitle = !empty($database_access->GetUsername($_GET['id'])) ? $database_access->GetUsername($_GET['id']) : "User Not Found.";
else $subtitle = "Invalid Page Request";
?>
    <title><?php print "{$title} - {$subtitle}";?></title>
</head>
<div id="wrapper" class="toggled">
    <?php include_once "./modules/navigation.php";?>
    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 content-bx bx-dark">
                    <?php $user = null;
                    if (isset($_SESSION['logged_in'])) {
                    }
                    if (!isset($_GET['user']) && !isset($_GET['id'])) print "<p class=\"font-weight-bold\">Invalid Page Request</p>";
                    else {
                        if (isset($_GET['user']) && count($database_access->GetUserProfile($_GET['user'])) > 0) {
                            $user = $database_access->GetUserProfile($_GET['user']);
                        } elseif (isset($_GET['id']) && count($database_access->GetUserProfileWithUserID($_GET['id'])) > 0) {
                            $user = $database_access->GetUserProfileWithUserID($_GET['id']);
                        }
                        if ($user != null) { ?>
                            <h2 class="noselect">
                                <span class="font-weight-normal">Profile of </span>
                                <?php print "{$user['last_name']}, {$user['first_name']}"; ?>
                            </h2>
                            <h5 class="noselect d-inline-block"><?php print $user['username']; ?></h5>
                            <h6 class="noselect d-inline-block font-weight-normal"> <?php print "({$user['user_id']})"; ?></h6>
                            <hr class="clearfix">
                            <?php $database_access->RenderTournamentsTable($database_access->GetTable("tournaments", ["owner_id" => $user['user_id']]));
                        } elseif ($user == null) print "<p class=\"font-weight-bold\">User Profile does not exist.</p>";
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
</div>
<?php ?>
<?php include_once "./modules/site.footer.php"; ?>
